#!/bin/bash
SRC_DIR="$1"
DST_DIR="$2"
BAUD="$3"
TEMPO="$4"
PITCH="$5"
RATE="$6"
SRC="$7"	

SRC_DIR=$(realpath "$SRC_DIR")
SRC=$(realpath "$SRC")
DST_DIR=$(realpath "$DST_DIR")
DST=${SRC/"$SRC_DIR"/"$DST_DIR"}
DST_DIR=$(dirname "$DST")
if test ! -f "$SRC"; then
	echo "Ошибка файл источник '$SRC' не может быть использован!"
	exit
fi
mkdir -p "$DST_DIR"
if test ! -d "$DST_DIR"; then
	echo "Ошибка при создании директории назначения!"
	exit
fi


lame --quiet --decode "$SRC" - |\
soundstretch stdin stdout -tempo="$TEMPO" -pitch="$PITCH" -rate="$RATE" |\
lame  --quiet -m a -cbr -b "$BAUD" --resample 22.05  - "$DST"
#id3cp -1 "$SRC" "$DST" #Копировалась кроказябра. Забил мне метадата всё равно не нужна
