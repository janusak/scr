#!/bin/bash

#stretch single file function
error(){
	echo "error:format: try \"stretchmp3 help\""
	exit
}


SRC="."
DST="./stretch"
BAUD="48"
TEMPO="+100"
PITCH="0"
RATE="+10"
PROC="$(nproc)" #по умолчанию колличество процессов по числу процессоров
SCRIPT_DIR=$(dirname $(readlink -e "$0"))

if test "$1" = help -o "$1" = ?; then
	cat "$SCRIPT_DIR"/stretchmp3.hlp | more
	exit
else
	#интерпретируем все переданные комманды
	k="1"
	f="$(eval "echo \$${k}")"
	while test -n "$f"
	do
		eval "$f"
		k=$[$k+1]
		f="$(eval "echo \$${k}")"
	done
fi

#$SRC должна быть папкой или файлом

if test -d "$SRC"  ; then 
	#xargs передаёт данные последним параметром
	find "$SRC" -name "*.mp3" -print0  |xargs -0 -P"$PROC" -L1\
	"$SCRIPT_DIR"/stretchmp3file.sh "$SRC" "$DST" "$BAUD" "$TEMPO" "$PITCH" "$RATE" 
elif test -f "$SRC" ; then 
	#сдесь мы работаетм с единичным файлом
	"$SCRIPT_DIR"/stretchmp3file.sh $(dirname $(realpath "./$SRC")) "$DST" "$BAUD" "$TEMPO" "$PITCH" "$RATE" "$SRC"
else
	echo "Error '${SRC}' is not file or directory!"
fi
